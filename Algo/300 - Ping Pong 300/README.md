## Problem
You are given a series of newline seperated integers as input. Write a program that follows these instructions for every integer.

If the integer is positive, remember it.
If the integer is 0, then print and forget the earliest number you remember.
Make sure to print a newline after every integer that you print.
If you try and print but the memory is empty, print 0.
Your code ends when -1 is inputted.

Netcat to 104.131.90.29:8008. 

Problem Writer: Uday Shankar

## Flag
`PP_wow_we_totally_didnt_give_you_the_flag`

Solved By: Noon

### Solution
See solution.pp
