## Problem
Keith has encountered a wild [Ping Pong](https://docs.google.com/document/d/1EfiFLSn6v1tF1iMG65MgZMJTgZ_cDWo8lEJUwomnmDo/edit?usp=sharing) [interpreter](https://play.hsctf.com/hsctf-static/PingPongInterpreters_b6500b7944b38b7d56326fd20298694564b90f879fca02dd1d4ecf4af5785334.zip) on their hike into the woods. The interpreter will only let them complete their hike if you write a program to find fibonacci numbers! Can you help Keith?

Your program must read a number n from input and then print out the nth Fibonacci number. For the purposes of this problem, the first, second, and third numbers of the Fibonacci sequence are 1, 1, 2. n will be in [1, 46], so that fib(n) will be < 2^31 - 1.

Netcat to 104.131.90.29:8007. 

Problem Writer: Jakob Degen

## Flag
`PP_0234FB97AA342D`

Solved By: Noon

### Solution
See solution.pp
