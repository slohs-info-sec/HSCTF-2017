## Problem
Keith is on a scavenger hunt in a large field. Each item on the scavenger hunt list has different point values, and the person who gets the most points in under an hour wins the grand prize. Keith found out from a secret source that the organizers of the scavenger hunt put the items with the highest point values on the edges of the convex polygonal field (assume the field is minimal around the provided points), so they decided to visit these locations only. The top-secret location of all the items is represented by the coordinates in a [.txt file](https://play.hsctf.com/hsctf-static/text_47d911e15cbf5df5aa121b2cf81156a9974bb3e6bbdf3a7a864158864db5ae4f.txt). Help Keith find the locations they need to travel to win! The flag is the product of the coordinates mod 10^9 + 7. 

Problem Writer: Rishitha Thambireddy

## Flag
`453644876`

Solved By: Noon

### Solution
>Ran a pre-made algorithm called "Graham Scan"  
Input points  
Multiplied it all together
