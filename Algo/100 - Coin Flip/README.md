## Problem
Keith has been very bored with his job as an industrial couponer lately, and so he has decided to spend his time flipping coins. The results of his coin flips are [in this file](https://play.hsctf.com/hsctf-static/data_6162614da1c7516eb36bc61fe5ddecd3706879a5b0c87120f7ada4739f5c4942.txt). Keith now wants to know how many runs of flips he found. A run is any consecutive sequence of the same flip. For example, the flips 001111101011 have three runs of length one, two runs of length two, and one run of length five. Can you help Keith count runs? The flag is the number of runs of length one, the number of runs of length two, the number of runs of length three, etc. up to the longest run in the sequence, each separated by a comma and space. 

Problem Writer: Jakob Degen

## Flag
`249368, 124813, 62558, 31389, 15475, 7891, 3975, 1982, 943, 486, 270, 107, 64, 33, 15, 8, 4, 1, 1, 1`

Solved By: Noon and Ganden (miscommunicated)

### Solution
See script
