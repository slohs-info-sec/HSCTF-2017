flips = []

with open('./data_6162614da1c7516eb36bc61fe5ddecd3706879a5b0c87120f7ada4739f5c4942.txt') as f:
	for line in f:
		for ch in line:
			flips.append(bool(int(ch)))

# print(flips)

runs = [0] * len(flips)
curRun = 0
curRunType = flips[0]
for i in range(len(flips)):
	if curRunType == flips[i]:
		curRun += 1
	else:
		runs[curRun] += 1
		curRun = 1
		curRunType = flips[i]

highestRun = 0
for i in range(len(runs)):
	if runs[i] != 0:
		highestRun = i

print(runs[1:highestRun + 1])
