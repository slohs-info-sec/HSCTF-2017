## Problem
Keith recently coded a [small authorization software](https://play.hsctf.com/hsctf-static/Keith_64823dfacef873e85d412ca2b2dd887bb5196a598901a3f98fc6d46e1ac633f9.class) for his/her computer to hide personal files. Unfortunately, he/she hit his head and forgot his/her password. Now he/she must reverse engineer his/her software to regain his/her password. 

Problem Writer: Jay Zhou

## Flag
`this_was_a_short_and_easy_problem_2B3F5AC0`

Solved By: Ben

### Solution
See script
