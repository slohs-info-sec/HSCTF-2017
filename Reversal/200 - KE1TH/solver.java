/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hsctf2017;

import java.util.Arrays;
import java.io.PrintStream;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64.Decoder;
import java.util.Scanner;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
public class HSCTF2017 {

    /**
     * @param args the command line arguments
     */
  private static Cipher aes;
  private static SecretKey key;
  private static byte[] iv = { 10, -73, -33, -65, 87, 87, -121, -41, -16, 89, 12, 31, 7, 82, -43, -100 };
    public static void main(String[] args) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {
    aes = Cipher.getInstance("AES/CBC/PKCS5Padding");
    byte[] bkey = java.util.Base64.getDecoder().decode("/Vl4PKzS9d+Vm/0eePmaYw==");
        //System.out.println(Arrays.toString(x));
        key = new SecretKeySpec(bkey, 0, bkey.length, "AES");
    aes.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
    byte [] x = string_to_bytes("-93^35^23^82^-4^57^-128^83^-95^-60^-100^73^40^-86^7^73^-101^3^118^-66^-104^69^121^76^1^-124^-124^-1^-64^29^28^43^2^-25^54^52^-79^-62^11^-43^52^-72^-117^-25^-103^-55^75^-97^");
    System.out.println(new String(aes.doFinal(x)));
    }
    public static int charCounter(String s){
        int total = 0;
        for (int i = 0; i < s.length(); i ++){
            if (s.charAt(i) == '^'){
                total ++;
            }
        }
        return total;
    }
    public static byte[] string_to_bytes (String s){
        byte [] bytes = new byte[charCounter(s)];
        int beg = 0;
        int end = s.indexOf("^", beg);
        for (int i = 0; i < bytes.length; i ++){
            if (beg <= s.length()){
                bytes[i] = Byte.parseByte(s.substring(beg, end));
                beg = end + 1;
                end = s.indexOf("^", beg);
            }
        }
        return bytes;
    }

}
