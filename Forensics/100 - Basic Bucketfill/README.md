## Problem
Keith is in a contest with another colleague, to see who can find the hidden message [in the picture](https://play.hsctf.com/hsctf-static/BasicBucketfill_4340353f7de921c14e3e42739198f4d2a84f6bc0ee15cbeac9fc98729e5c4002.png). However, they're only allowed to use the most basic editing tools!
Can you find out the secret message?  

Problem Writer: Sammy Berger

## Flag
`HHA8`

Solved By: Kaz

### Solution:
Paint bucket, or change the contrast
