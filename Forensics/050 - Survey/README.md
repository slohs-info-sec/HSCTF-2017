## Problem
A [quick survey](https://docs.google.com/forms/d/e/1FAIpQLScHLVn6y66s7qrb0-MXXi0W8p8r8Z9E_mcflsq8U9k6x3Fw2g/viewform?usp=sf_link) worth 50 points! 

Problem Writer: Jakob Degen

## Flag
`I love surveys, who doesn't love surveys, let's all take a survey!`

Solved By: Kaz

### Solution
Complete the survey
