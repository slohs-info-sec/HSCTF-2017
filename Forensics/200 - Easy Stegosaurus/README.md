## Problem
Keith infiltrated the scary evil organization ZORE, and had to fight a cloned stegosaurus!

After killing the evil beast, he started to dissect the body for potential information. He found a usb containing two similar looking image files: one called [logo.png](https://play.hsctf.com/hsctf-static/logo_f99691b87b965f85c4b3a3c877667dae41a622b28a2854961e81c1110e0958b3.png), and one called [changed.png](https://play.hsctf.com/hsctf-static/changed_bc45f1089a5a88a6b429aa6f8b8a6954e71c02d45aab836b4fb43d34c34d563f.png).

What secret information could be contained in these strange files? 

Problem Writer: Sammy Berger

## Flag
`sammyshouldworkmore`

Solved By: Ganden

### Solution
Compare the images using something like [Resemble.js](https://huddle.github.io/Resemble.js/)
