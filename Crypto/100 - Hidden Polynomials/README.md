## Problem
Alice is sending a top secret polynomial, P(x), to Bob. You want to know the equation of P(x). In your attempts to intercept their message, you discover only two facts about P(x):

It’s coefficients are in the set {0, 1, 2, …, 1000}.  
P(2017) = 49189926321482294101673925793095

The flag will be P(1). 

Problem Writer: Alan Yan

## Flag
`231`

Solved By: Noon

### Solution
`P(x) = 1 + (2x^1) + (3x^2) + (5x^3) + (8x^4) + (13x^5) + (21x^6) + (34x^7) + (55x^8) + (89*x^9)`
>Used a high precision calculator, literally subtracted different polynomials from the number they gave  
Found out x^9 was the highest it could be without going negative  
Then kept trying different coefficients from 1000 to 0 in front
