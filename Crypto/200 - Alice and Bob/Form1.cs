﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;

namespace DiffieHellman
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void cmdCalculate_Click(object sender, EventArgs e)
        {
            BigInteger cryptomod = BigInteger.Parse(txtModulus.Text);
            BigInteger cryptobase = BigInteger.Parse(txtBase.Text);
            BigInteger ASends = BigInteger.Parse(txtASends.Text);
            BigInteger BSends = BigInteger.Parse(txtBSends.Text);

            BigInteger exponent = 0;
            BigInteger result = 0;
            while(result != ASends){
                exponent++;
                result = BigInteger.ModPow(cryptobase, exponent, cryptomod);
            }
            txtResult.Text = exponent.ToString();
            txtShared.Text = BigInteger.ModPow(BSends, exponent, cryptomod).ToString();
        }
    }
}
